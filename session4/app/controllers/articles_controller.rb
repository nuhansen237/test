class ArticlesController < ApplicationController
  
  def welcome
    render :text => "Welcome to Ruby on Rails"
  end
  def new
    @article = Article.new  
  end
  def index
    @article = Article.all
  end
  def edit
    @article = Article.find(params[:id])
  end
  def update
     @article = Article.find(params[:id])
     if @article.update_attributes(params[:article])
     redirect_to articles_path, :notice => 'Article was successfully updated.'
   else
     flash[:error] = 'Article was failed to update.'
     render :action => 'edit'
   end
  end
  def create
    @article= Article.new(params[:article])
    if @article.save
      flash[:notice] = "Article successfully saved"
      redirect_to :action => :index
    else
      flash.now[:notice] = "Article failed to saved"
      render :action => :new
    end
  end
  
  def destroy
    Article.destroy(params[:id])
    redirect_to articles_path, :notice => 'Article was successfully deleted'
  end
  
  def show
  @articles = Article.find(params[:id])
  respond_to do |format|
    format.html  # show.html.erb
    format.json  { render :json => @articles }
  end
end

end
