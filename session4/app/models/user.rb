class Users < ActiveRecord::Base
  has_many :article, :dependent => :destroy
  has_many :product, :dependent => :destroy
  has_one :country
  has_many :my_country_article, 
           :class_name => "article", 
           :foreign_key => "user_id",
           :conditions => "title like '%my country%'"
           
  
  validates :first_name, :presence => true,
                         :uniqueness => true,
                         :length => {:minimum => 1, :maximum => 20},
                         :format => {:with => /[a-zA-Z\s]+$/}
  validates :last_name,  :presence => true,
                         :uniqueness => true,
                         :length => {:minimum => 1, :maximum => 20},
                         :format => {:with => /[a-zA-Z\s]+$/}
  validates :email, :presence => true,
                    :uniqueness => true
                      
  def full_address
    #self disini adalah object yang memanggil method tersebut.
    "#{self.address} #{self.last_name}"
  end
  
  
end 
