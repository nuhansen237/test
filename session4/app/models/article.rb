class Article < ActiveRecord::Base
  belongs_to :user
  has_many :comment, :dependent => :destroy
  
  scope :rating_is_or_above, lambda {|rating| where("rating > ?", rating)}
  
  validates :title, :presence => true,
                    :uniqueness => true
end
