class Countries < ActiveRecord::Base
  belongs_to :users
  
  validates :code, :presence => true,
                   
  validates :name, :presence => true,
                   :length => {:minimum => 1, :maximum => 15}
  
  validate :valid_code
  def valid_code
   self.errors[:code] << "only filled id,usa,frc" if (code != "id" && code != "usa" && code!= "frc")
  end  

end
