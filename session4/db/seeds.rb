# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = Users.create([{
  :first_name => "user1",
  :last_name => "satu",
  :email => "user1@yahoo.com",
  :username => "user1",
  :address => "jl. bandung1",
  :age => 20, 
  :birthday => "1990-01-01", 
  },{
  :first_name => "user2",
  :last_name => "dua",
  :email => "user2@yahoo.com",
  :username => "user2",
  :address => "jl. bandung2",
  :age => 20, 
  :birthday => "1990-01-02", 
  },{
  :first_name => "user3",
  :last_name => "tiga",
  :email => "user3@yahoo.com",
  :username => "user3",
  :address => "jl. bandung3",
  :age => 20, 
  :birthday => "1990-01-03", 
  },{
  :first_name => "user4",
  :last_name => "empat",
  :email => "user4@yahoo.com",
  :username => "user4",
  :address => "jl. bandung4",
  :age => 20, 
  :birthday => "1990-01-04", 
  },{
  :first_name => "user5",
  :last_name => "lima",
  :email => "user5@yahoo.com",
  :username => "user5",
  :address => "jl. bandung5",
  :age => 20, 
  :birthday => "1990-01-05", 
  }])
  
